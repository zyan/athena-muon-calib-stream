from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.AccumulatorCache import AccumulatorCache


def MuonCalibStreamReadCfg(flags):
    """Set up to read from a muon calibration stream bytestream file

    The function adds the components required to read events and metadata from
    bytestream input. May be used to read events from a secondary input as well
    primary input file.

    Args:
        flags:      Job configuration flags
        Fix the type_names for MDT, TGC and RPC in this configuration
        
    Services:
        MuonCalibStreamCnvSvcCfg
        MuonCalibStreamDataProviderSvcCfg
        MuonCalibStreamFileInputSvcCfg
        MuonCalibStreamAddressProviderSvcCfg
        EventSelectorMuonCalibStreamCfg
        MuonCalibRunLumiBlockCoolSvcCfg (to be fixed)

    Returns:
        A component accumulator fragment containing the components required to read 
        from Muon calibration stream bytestream data. Should be merged into main job configuration.
    """
    result = ComponentAccumulator()

    # import the basic fragment
    from MuonConfig.MuonGeometryConfig import MuonIdHelperSvcCfg,MuonGeoModelCfg
    result.merge(MuonIdHelperSvcCfg(flags))
    result.merge(MuonGeoModelCfg(flags))
    
    from MuonCalibStreamCnvSvc.MuonCalibStreamCnvSvcConfig import MuonCalibStreamCnvSvcCfg,\
                                                                MuonCalibStreamDataProviderSvcCfg,\
                                                                MuonCalibStreamFileInputSvcCfg,\
                                                                MuonCalibStreamAddressProviderSvcCfg,\
                                                                EventSelectorMuonCalibStreamCfg,\
                                                                MuonCalibRunLumiBlockCoolSvcCfg
    result.merge(MuonCalibStreamCnvSvcCfg(flags))

    # set MuonCalibStream EvtPersistencySvc
    event_persistency = CompFactory.EvtPersistencySvc(name="EventPersistencySvc", CnvServices=["MuonCalibStreamCnvSvc"])
    result.addService(event_persistency)

    # set MuonCalibStream AddressProviderSvc with default MDT,TGC and RPC type_names, add STGC and MM for avoiding erros 
    # type_names: (optional) specific type names for address provider to find
    type_names = ['Muon::MdtPrepDataContainer/MDT_DriftCircles', 'RpcPadContainer/RPCPAD', 'TgcRdoContainer/TGCRDO']
                
    address_provider = CompFactory.MuonCalibStreamAddressProviderSvc(TypeNames=type_names if type_names else list(),)
    result.addService(address_provider)

    # set MuonCalibStream ProxyProviderSvc
    proxy = CompFactory.ProxyProviderSvc(ProviderNames = [address_provider.name])
    result.addService(proxy)

     # set MuonCalibrationStream event selector 
    result.merge(EventSelectorMuonCalibStreamCfg(flags, name = 'EventSelector'))
    result.setAppProperty("EvtSel", "EventSelector")

    # import other MuonCalibStreamCnv Services 
    result.merge(MuonCalibStreamFileInputSvcCfg(flags))
    result.merge(MuonCalibStreamDataProviderSvcCfg(flags))
    result.merge(MuonCalibRunLumiBlockCoolSvcCfg(flags))

    # set the RDO to PRD tools for TGC and RPC (MDT doesn't need this step)
    from MuonConfig.MuonRdoDecodeConfig import RpcRDODecodeCfg,TgcRDODecodeCfg
    if flags.Detector.GeometryRPC:
        result.merge(RpcRDODecodeCfg(flags))
    if flags.Detector.GeometryTGC:
        result.merge(TgcRDODecodeCfg(flags))

    return result


def MuonCalibStreamTestAlgCfg(flags, name = "MuonCalibStreamTestAlg", **kwargs):
    result = ComponentAccumulator()
    
    from MuonConfig.MuonGeometryConfig import MuonIdHelperSvcCfg,MuonGeoModelCfg
    result.merge(MuonIdHelperSvcCfg(flags))
    result.merge(MuonGeoModelCfg(flags))
    # from MuonConfig.MuonPrepDataConvConfig import MuonPrepDataConvCfg
    # result.merge(MuonPrepDataConvCfg(flags))

    result.addEventAlgo(CompFactory.MuonCalibStreamTestAlg(name, ByCont = 1, ByColl = 0, **kwargs))
    
    return result


