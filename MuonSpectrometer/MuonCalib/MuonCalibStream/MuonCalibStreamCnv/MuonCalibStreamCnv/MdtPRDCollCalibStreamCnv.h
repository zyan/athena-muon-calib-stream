/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/
#ifndef MUONCALIBSTREAM_MDTPRDCOLLCALIBSTREAMCNV_H
#define MUONCALIBSTREAM_MDTPRDCOLLCALIBSTREAMCNV_H

#include <stdint.h>

#include <map>

#include "AthenaBaseComps/AthConstConverter.h"
#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/ToolHandle.h"
#include "MdtCalibSvc/MdtCalibrationTool.h"
#include "MuonCalibStreamCnvSvc/MuonCalibStreamDataProviderSvc.h"
#include "MuonPrepRawData/MdtPrepDataCollection.h"
#include "MuonReadoutGeometry/MuonDetectorManager.h"

// Externals
extern long MuonCalibStream_StorageType;

class MdtPRDCollCalibStreamCnv : public AthConstConverter {
public:
    StatusCode initialize() override;
    StatusCode createObjConst(IOpaqueAddress *pAddr, DataObject *&pObj) const override;
    StatusCode createRepConst(DataObject *pObj, IOpaqueAddress *&pAddr) const override;

    /// Storage type and class ID
    virtual long repSvcType() const { return storageType(); }
    static long storageType() { return MuonCalibStream_StorageType; }
    static const CLID &classID();

    MdtPRDCollCalibStreamCnv(ISvcLocator *svcloc);
    ~MdtPRDCollCalibStreamCnv() = default;

private:
    StatusCode fillCollections(Muon::MdtPrepDataCollection *&mdtCollection, const unsigned long *ipar) const;

    ServiceHandle<IMuonCalibStreamDataProviderSvc> m_dataProvider{"MdtPRDCollCalibStreamCnv", name()};
    const MuonGM::MuonDetectorManager *m_muonMgr{nullptr};
    ToolHandle<MdtCalibrationTool> m_calib_tool{"MdtCalibrationTool"};
    const MdtIdHelper *m_mdtIdHelper{nullptr};
};
#endif
