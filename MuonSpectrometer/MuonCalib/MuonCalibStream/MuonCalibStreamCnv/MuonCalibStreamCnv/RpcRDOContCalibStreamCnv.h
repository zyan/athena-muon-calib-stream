#ifndef MUONCALIBSTREAM_RPCRDOCONTCALIBSTREAMCNV_H
#define MUONCALIBSTREAM_RPCRDOCONTCALIBSTREAMCNV_H

#include <stdint.h>
#include <map>
#include "GaudiKernel/Converter.h"
#include "AthenaBaseComps/AthMessaging.h"
#include "MuonReadoutGeometry/MuonDetectorManager.h"
#include "MuonPrepRawData/MuonPrepDataContainer.h"
#include "MuonRDO/RpcPadContainer.h"
#include "RPC_CondCabling/RpcCablingCondData.h"
#include "StoreGate/ReadCondHandleKey.h"
#include "MuonIdHelpers/IMuonIdHelperSvc.h"
#include "MuonCalibStreamCnvSvc/IMuonCalibStreamDataProviderSvc.h"

class DataObject;
class StatusCode;
class StoreGateSvc; 
class MuonCalibStreamDataProviderSvc;

#include <string>

// Abstract factory to create the converter
template <class TYPE> class CnvFactory;

// Externals 
extern long MuonCalibStream_StorageType;

class RpcRDOContCalibStreamCnv: public Converter, public AthMessaging {
  friend class CnvFactory<RpcRDOContCalibStreamCnv>;

 public:
  RpcRDOContCalibStreamCnv(ISvcLocator *svcloc);
  ~RpcRDOContCalibStreamCnv();
  virtual StatusCode initialize();
  virtual StatusCode createObj(IOpaqueAddress *pAddr, DataObject *&pObj); 
  virtual StatusCode createRep(DataObject *pObj, IOpaqueAddress *&pAddr);

  /// Storage type and class ID
  virtual long repSvcType() const { return MuonCalibStream_StorageType; }
  static long storageType()     { return MuonCalibStream_StorageType; }
  static const CLID& classID();

 private: 
  StatusCode fillCollections(DataObject *&pObj);

  Muon::RpcPrepDataContainer *m_container; 
  StoreGateSvc *m_storeGate; 
  MuonCalibStreamDataProviderSvc *m_dataProvider;
  const MuonGM::MuonDetectorManager *m_muonMgr;
  ServiceHandle<Muon::IMuonIdHelperSvc> m_idHelperSvc;
  SG::ReadCondHandleKey<RpcCablingCondData> m_readKey;

  //const RpcIdHelper *m_rpcIdHelper;
  //  const IRPCcablingSvc *m_cabling;
  const RpcCablingCondData *m_cabling;
  RpcPadContainer *m_padContainer;
  //  int m_rpcOffset;
  //  rel23 fix
  // SG::ReadCondHandleKey<RpcCablingCondData> m_readKey{this, "ReadKey", "RpcCablingCondData", "Key of RpcCablingCondData"};
  // ServiceHandle<Muon::IMuonIdHelperSvc> m_idHelperSvc{this, "MuonIdHelperSvc", "Muon::MuonIdHelperSvc/MuonIdHelperSvc"};
  // ServiceHandle<IMuonCalibStreamDataProviderSvc> m_dataProvider{this, "DataProviderSvc", "MuonCalibStreamDataProviderSvc"};
  // const MuonGM::MuonDetectorManager* m_muonMgr{nullptr};

};
#endif
