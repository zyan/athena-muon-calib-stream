/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/
#ifndef MUONCALIBSTREAM_MDTPRDCONTCALIBSTREAMCNV_H
#define MUONCALIBSTREAM_MDTPRDCONTCALIBSTREAMCNV_H

#include <stdint.h>

#include <map>

#include "AthenaBaseComps/AthMessaging.h"
#include "GaudiKernel/Converter.h"
#include "GaudiKernel/ServiceHandle.h"
#include "MuonPrepRawData/MuonPrepDataContainer.h"
#include "MuonReadoutGeometry/MuonDetectorManager.h"
#include "MuonIdHelpers/IMuonIdHelperSvc.h"

class DataObject;
class StatusCode;
class StoreGateSvc;
class MuonCalibStreamDataProviderSvc;
class MdtCalibrationTool;

#include <string>

// Abstract factory to create the converter
template <class TYPE> class CnvFactory;

// Externals
extern long MuonCalibStream_StorageType;

class MdtPRDContCalibStreamCnv : public Converter, public AthMessaging {
    friend class CnvFactory<MdtPRDContCalibStreamCnv>;

public:
    MdtPRDContCalibStreamCnv(ISvcLocator *svcloc);
    ~MdtPRDContCalibStreamCnv();
    virtual StatusCode initialize();
    virtual StatusCode createObj(IOpaqueAddress *pAddr, DataObject *&pObj);
    virtual StatusCode createRep(DataObject *pObj, IOpaqueAddress *&pAddr);

    /// Storage type and class ID
    virtual long repSvcType() const { return MuonCalibStream_StorageType; }
    static long storageType() { return MuonCalibStream_StorageType; }
    static const CLID &classID();

private:
    StatusCode fillCollections(IOpaqueAddress *pAddr, DataObject *&pObj);
    Muon::MdtPrepDataContainer *m_container;
    StoreGateSvc *m_storeGate;
    MuonCalibStreamDataProviderSvc *m_dataProvider;
    const MuonGM::MuonDetectorManager *m_muonMgr;
    ServiceHandle<Muon::IMuonIdHelperSvc> m_idHelperSvc;
    //  MdtCalibrationTool *m_calibSvc;
};
#endif
