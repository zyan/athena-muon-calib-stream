#ifndef MUONCALIBSTREAMTESTALG_H
#define MUONCALIBSTREAMTESTALG_H

#include "AthenaBaseComps/AthAlgorithm.h"
#include "MuonIdHelpers/MdtIdHelper.h"


//  Test Algoritm to dump calibration stream data
//  Dumps MDT and RPC data, CSC, TGC not implemented.
class MuonCalibStreamTestAlg : public AthAlgorithm {
public:
    MuonCalibStreamTestAlg(const std::string &name, ISvcLocator *pSvcLocator);
    virtual ~MuonCalibStreamTestAlg();
    virtual StatusCode initialize() override;
    virtual StatusCode execute() override;
    virtual StatusCode finalize() override;

protected:
    ActiveStoreSvc *m_activeStore;
    //const MdtIdHelper *m_mdtIdHelper;

private:
    int m_byCont;  // flag to dump data by container
    int m_byColl;  // flag to dump data by collection
    //SG::ReadHandleKey<Muon::MdtPrepDataContainer> m_MdtPrepData {this, "MdtPrepData", "MDT_DriftCircles"};
};
#endif
