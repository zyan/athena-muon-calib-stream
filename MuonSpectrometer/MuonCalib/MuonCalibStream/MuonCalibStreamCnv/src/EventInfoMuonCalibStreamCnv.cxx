/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/
#include "MuonCalibStreamCnv/EventInfoMuonCalibStreamCnv.h"

#include <time.h>
#include <iostream>
#include <memory>

#include "AthenaBaseComps/AthCheckMacros.h"
#include "AthenaBaseComps/AthMsgStreamMacros.h"
#include "EventInfo/EventID.h"
#include "EventInfo/EventInfo.h"
#include "EventInfo/EventType.h"
#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/IRegistry.h"
#include "GaudiKernel/StatusCode.h"
#include "MuCalDecode/CalibEvent.h"
#include "MuonCalibStreamCnvSvc/MuonCalibStreamAddress.h"
#include "MuonCalibStreamCnvSvc/MuonCalibStreamCnvSvc.h"
#include "MuonCalibStreamCnvSvc/MuonCalibStreamDataProviderSvc.h"
#include "MuonCalibStreamCnvSvc/MuonCalibStreamInputSvc.h"
#include "AthenaKernel/StorableConversions.h"

// Instantiation of a static factory class used by clients to create
// instances of this service
// static CnvFactory<EventInfoMuonCalibStreamCnv> s_factory;
// const  ICnvFactory& EventInfoMuonCalibStreamCnvFactory = s_factory;

EventInfoMuonCalibStreamCnv::EventInfoMuonCalibStreamCnv(ISvcLocator *svcloc) :
    Converter(MuonCalibStream_StorageType, classID(), svcloc),
    AthMessaging(msgSvc(), "EventInfoMuonCalibStreamCnv"),
    m_MuonCalibStreamCnvSvc(0),
    m_dataProvider(0)
{}

const CLID &EventInfoMuonCalibStreamCnv::classID() { return ClassID_traits<EventInfo>::ID(); }

StatusCode EventInfoMuonCalibStreamCnv::initialize() {
    ATH_MSG_DEBUG("Initialize EventInfoMuonCalibStreamCnv");

    ATH_CHECK(Converter::initialize());

    // Check MuonCalibStreamCnvSvc
    IService *svc;
    ATH_CHECK(serviceLocator()->getService("MuonCalibStreamCnvSvc", svc));

    m_MuonCalibStreamCnvSvc = dynamic_cast<MuonCalibStreamCnvSvc *>(svc);
    if (m_MuonCalibStreamCnvSvc == 0) {
        ATH_MSG_ERROR(" Cannot cast to  MuonCalibStreamCnvSvc ");
        return StatusCode::FAILURE;
    }

    ATH_CHECK(serviceLocator()->getService("MuonCalibStreamDataProviderSvc", svc));

    m_dataProvider = dynamic_cast<MuonCalibStreamDataProviderSvc *>(svc);
    if (m_dataProvider == 0) {
        ATH_MSG_ERROR(" Cannot cast to MuonCalibStreamDataProviderSvc ");
        return StatusCode::FAILURE;
    }

    return StatusCode::SUCCESS;
}

StatusCode EventInfoMuonCalibStreamCnv::createObj(IOpaqueAddress *pAddr, DataObject *&pObj) {
    static unsigned int timeStamp_previous(0);  // save previous timeStamp to use in case timeStamp=0

    MuonCalibStreamAddress *pRE_Addr;
    pRE_Addr = dynamic_cast<MuonCalibStreamAddress *>(pAddr);
    if (!pRE_Addr) {
        ATH_MSG_ERROR(" Cannot cast to MuonCalibStreamAddress ");
        return StatusCode::FAILURE;
    }

    ATH_MSG_DEBUG(" Creating Objects   ");

    // get CalibEvent
    const LVL2_MUON_CALIBRATION::CalibEvent *re = m_dataProvider->getEvent();
    if (!re) {
        ATH_MSG_ERROR(" Can not get CalibEvent ");
        return StatusCode::FAILURE;
    }

    // Event Type
    EventType *peT = new EventType();
    peT->add_type(EventType::IS_CALIBRATION);
    peT->set_user_type("IS_CALIBRATION");

    // Run Number
    // for the time being we handle the run number via the dataProvider
    //  int runNumber = re->run_number();
    int runNumber = m_dataProvider->fakeRunN();

    // Event Number
    // for the time being we handle the event number via the dataProvider
    //  int eventNumber = re->lvl1_id();
    int eventNumber = m_dataProvider->fakeEventN();

    // Time Stamp
    unsigned int timeStamp = re->timestamp();
    // 2105-05-20 (temporary?) fix for timeStamps which are zero.
    if (timeStamp == 0) {
        timeStamp = timeStamp_previous;
        ATH_MSG_INFO(" TIMESTAMP ZERO, run/event= " << runNumber << " " << eventNumber << " timestamp=" << timeStamp
                                                    << " previous timestamp=" << timeStamp_previous
                                                    << " previous Date = " << ascTime(timeStamp_previous));
    } else {
        timeStamp_previous = timeStamp;
    }
    int lumiBlock = m_dataProvider->fakeLumiBlock();

    pObj = SG::asStorable(std::make_unique<EventInfo>(new EventID(runNumber, eventNumber, timeStamp, 0, lumiBlock, 0), peT));

    // PRINT TIMESTAMP INFO
    ATH_MSG_DEBUG("New EventInfo made, run/event/date= "<<runNumber<<" "<<eventNumber<<" "<<ascTime(timeStamp));

    return StatusCode::SUCCESS;
}  // EventInfoMuonCalibStreamCnv::createObj()

// Fill RawEvent object from DataObject
// (but really just configure the message service)
StatusCode EventInfoMuonCalibStreamCnv::createRep(DataObject * /* pObj */, IOpaqueAddress *& /* pAddr */) {
    ATH_MSG_DEBUG(" Nothing to be done for EventInfo createRep ");
    return StatusCode::SUCCESS;
}

// Convert (unix) timestamp to a (human-readable) calendar date
const char *EventInfoMuonCalibStreamCnv::ascTime(unsigned int tstamp) {
    struct tm t;

    t.tm_sec = tstamp;
    t.tm_min = 0;
    t.tm_hour = 0;
    t.tm_mday = 0;
    t.tm_mon = 0;
    t.tm_year = 70;
    t.tm_wday = 00;
    t.tm_yday = 00;
    t.tm_isdst = 0;

    time_t ct = mktime(&t);
    tm *t2 = gmtime(&ct);

    return asctime(t2);
}
