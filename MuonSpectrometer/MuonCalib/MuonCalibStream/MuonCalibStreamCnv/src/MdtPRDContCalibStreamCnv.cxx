/*
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
*/
#include "MuonCalibStreamCnv/MdtPRDContCalibStreamCnv.h"

#include "AthenaBaseComps/AthCheckMacros.h"
#include "AthenaBaseComps/AthMsgStreamMacros.h"
#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/IRegistry.h"
#include "GaudiKernel/StatusCode.h"
#include "MuCalDecode/CalibData.h"
#include "MuCalDecode/CalibEvent.h"
#include "MuCalDecode/CalibUti.h"
#include "MuonCalibEvent/MdtCalibHit.h"
#include "MuonCalibStreamCnvSvc/MuonCalibStreamAddress.h"
#include "MuonCalibStreamCnvSvc/MuonCalibStreamDataProviderSvc.h"
#include "MuonPrepRawData/MuonPrepDataCollection.h"
//#include "MdtCalibSvc/MdtCalibrationTool.h"
#include <iostream>
#include <list>
#include <map>

#include "MuonReadoutGeometry/MdtReadoutElement.h"
#include "AthenaKernel/StorableConversions.h"

using namespace LVL2_MUON_CALIBRATION;

// function to sort the Mdt hits according to the LVL2 identifier
bool CompareIds(const MdtCalibData data1, const MdtCalibData data2) { return data1.id() > data2.id(); }

MdtPRDContCalibStreamCnv::MdtPRDContCalibStreamCnv(ISvcLocator *svcloc) :
    Converter(MuonCalibStream_StorageType, classID(), svcloc),
    AthMessaging(msgSvc(), "MdtPRDContCalibStreamCnv"),
    m_container(nullptr),
    m_storeGate(nullptr),
    m_dataProvider(nullptr),
    m_muonMgr(nullptr),
    m_idHelperSvc("MuonIdHelperSvc","Muon::MuonIdHelperSvc/MuonIdHelperSvc")
//, m_calibSvc(nullptr) 
{}

MdtPRDContCalibStreamCnv::~MdtPRDContCalibStreamCnv() {}

const CLID &MdtPRDContCalibStreamCnv::classID() { return ClassID_traits<Muon::MdtPrepDataContainer>::ID(); }

StatusCode MdtPRDContCalibStreamCnv::initialize() {
    ATH_MSG_INFO("Initialize MdtPRDContCalibStreamCnv");

    ATH_CHECK(Converter::initialize());
    // Retrieve the idHelper
    ATH_CHECK(m_idHelperSvc.retrieve());
    // Get the muon detector descriptor manager
    StoreGateSvc *detStore = nullptr;
    ATH_CHECK(serviceLocator()->service("DetectorStore", detStore));
    ATH_CHECK(detStore->retrieve(m_muonMgr));
    if (m_muonMgr == nullptr) {
        ATH_MSG_ERROR("Null pointer to the read MuonDetectorManager conditions object");
        return StatusCode::FAILURE;
    }
    // retrieve the dataProviderSvc
    IService *svc;
    ATH_CHECK(serviceLocator()->getService("MuonCalibStreamDataProviderSvc", svc));

    m_dataProvider = dynamic_cast<MuonCalibStreamDataProviderSvc *>(svc);
    if (m_dataProvider == 0) {
        ATH_MSG_ERROR("Cannot cast to MuonCalibStreamDataProviderSvc");
        return StatusCode::FAILURE;
    }

    // get MDT Calibration service
    // ATH_CHECK( serviceLocator()->service("MdtCalibrationSvc", m_calibSvc) );

    ATH_CHECK(service("StoreGateSvc", m_storeGate));

    return StatusCode::SUCCESS;
}  // MdtPRDContCalibStreamCnv::initialize()

StatusCode MdtPRDContCalibStreamCnv::createObj(IOpaqueAddress *pAddr, DataObject *&pObj) {
    //  typedef Muon::MdtPrepDataContainer::ID CollID
    MuonCalibStreamAddress *pRE_Addr;
    pRE_Addr = dynamic_cast<MuonCalibStreamAddress *>(pAddr);
    if (!pRE_Addr) {
        ATH_MSG_ERROR("Cannot cast to MuonCalibStreamAddress");
        return StatusCode::FAILURE;
    }

    // get name of
    const std::string &nm = *(pRE_Addr->par());

    // create MdtPRDContainer if it does not exist
    if (!m_container) {
        ATH_MSG_DEBUG("Creating MdtPrepDataContainer of size " << m_idHelperSvc->mdtIdHelper().module_hash_max());

	m_container = new Muon::MdtPrepDataContainer(m_idHelperSvc->mdtIdHelper().module_hash_max());
	m_container->addRef();

        //   m_container->setAllCollections(m_idHelperSvc->mdtIdHelper().module_hash_max());

        // create all address Objects
        //   const MuonDetElemHash& hashF = m_container->hashFunc();
        //   int n = haxshF.max();

        //   ATH_MSG_DEBUG( "max number of hash ids: " << n );

        // Loop over collection identifiers - basically a loop over MDT chambers
        // This makes an address in SG for MdtPrepData objects, one per chamber
        MdtIdHelper::const_id_iterator id_it = m_idHelperSvc->mdtIdHelper().module_begin();
        for (int i = 0; id_it != m_idHelperSvc->mdtIdHelper().module_end(); id_it++, i++) {
	    ATH_MSG_DEBUG("Identifier is " << (*id_it).getString());

	    // reverse lookup.
	    // Muon::MdtPrepDataCollection::ID collID = *id_it;
	    // ATH_MSG_DEBUG( "Identifier is " << collID.getString() );

	    // Muon::MdtPrepDataContainer::KEY
	    // std::string nm= m_container->key(collID);

	    // create an Address object,
            MuonCalibStreamAddress *addr = new MuonCalibStreamAddress(ClassID_traits<Muon::MdtPrepDataCollection>::ID(), "", "", i, 0);
	    // register it in StoreGate.
            ATH_CHECK(m_storeGate->recordAddress(addr, true));
        }  // end loop over collection identifiers
    }      // end if !m_container

    m_container->cleanup();

    ATH_MSG_DEBUG("Creating Objects   " << nm);

    // m_container->setAllCollections();
    ATH_CHECK(fillCollections(pAddr, pObj));
    pObj = SG::asStorable(m_container);

    return StatusCode::SUCCESS;
}  // MdtPRDContCalibStreamCnv::createObj()

StatusCode MdtPRDContCalibStreamCnv::createRep(DataObject * /*pObj*/, IOpaqueAddress *& /*pAddr*/) {
    ATH_MSG_ERROR(" No conversion MdtPrepData to BS ");
    return StatusCode::FAILURE;
}

StatusCode MdtPRDContCalibStreamCnv::fillCollections(IOpaqueAddress * /*pAddr*/, DataObject *& /*pObj*/) {
    const CalibEvent *event = m_dataProvider->getEvent();

    // Skip events with no MDT hits (otherwise crash below)
    if (!event->mdt()) {
        ATH_MSG_WARNING(" NO MDT hits in event!");
        return StatusCode::SUCCESS;
    }

    // extract the list of MDT hit tubes
    std::list<MdtCalibData> tubes = (event->mdt())->data();
    // sort the list using CompareIds function
    tubes.sort(CompareIds);
    std::list<MdtCalibData>::const_iterator tube = tubes.begin();

    Muon::MdtPrepDataCollection *mdtCollection = nullptr;
    //    std::unique_ptr<Muon::MdtPrepDataCollection> mdtCollection = nullptr;  
    Identifier previousId, elementId;

    int mdt_hits(0), mdt_chambers(0);
    int StationName, StationEta, StationPhi, MultiLayer, TubeLayer, Tube;
    // loop over MDT hit tubes
    // assumes only leading tdc_counts is present on the data
    for (; tube != tubes.end(); tube++) {
        uint16_t coarse = (*tube).leadingCoarse();
        uint16_t fine = (*tube).leadingFine();
        int tdc_counts = fine | (coarse << 5);
        int adc_counts = (*tube).width();
        WordIntoMdtId((*tube).id(), StationName, StationEta, StationPhi, MultiLayer, TubeLayer, Tube);

        // convert the tube hit info to an MdtPrepData object
        Muon::MdtDriftCircleStatus digitStatus = Muon::MdtStatusDriftTime;
        Identifier channelId = m_idHelperSvc->mdtIdHelper().channelID(StationName, StationEta, StationPhi, MultiLayer, TubeLayer, Tube);

        // Fix ticksize for chambers with HPTDC (BMG, BIS7A)
        if( m_idHelperSvc->hasHPTDC(channelId) ) {
            tdc_counts /= 4.;
            adc_counts /= 4.;
            //      ATH_MSG_INFO("AFTER  " << channelId<<" "<<tdc_counts<<" "<<adc_counts<<" "<<(*tube).phi()<<" "<<
            //      StationName<<" "<< StationEta<<" "<< StationPhi<<" "<< MultiLayer<<" "<< TubeLayer<<" "<<Tube);
        }

        double radius(7.5), errRadius(4.2);
        const MuonGM::MdtReadoutElement *detEl = m_muonMgr->getMdtReadoutElement(channelId);
        // Commented out code just does preliminary radius calc, based only on TDC
        // This is unneeded because it is recalulated later with full corrections.
        //    Amg::Vector3D position = detEl->tubePos(channelId);
        //    double measured_perp = position.perp();
        //    if( detEl->getStationS() != 0. ) {
        //      measured_perp = sqrt(measured_perp*measured_perp - detEl->getStationS()*detEl->getStationS());
        //    }
        //    double measured_x = measured_perp*cos( position.phi() );
        //    double measured_y = measured_perp*sin( position.phi() );
        //    const Amg::Vector3D measured_position(measured_x, measured_y, position.z());
        //    MdtCalibHit calib_hit = MdtCalibHit( channelId, tdc_counts, adc_counts, measured_position, detEl );
        //    double signedTrackLength = measured_position.mag();
        //    m_calibSvc->driftRadiusFromTime(calib_hit, signedTrackLength);
        //    radius    = calib_hit.driftRadius();
        //    errRadius = calib_hit.sigmaDriftRadius();

	ATH_MSG_DEBUG(" "<<m_idHelperSvc->mdtIdHelper().stationNameString(StationName)<<"_"<< StationPhi<<"_"<< StationEta<<
		      "-"<<MultiLayer<<"-"<<TubeLayer<<"-"<<Tube<<" ADC="<<(*tube).width()<<" TDC="<<tdc_counts );
	//		      <<" "<<radius<<" OK="<<m_calibSvc->driftRadiusFromTime(calib_hit, signedTrackLength); );

        Amg::Vector2D driftRadius(radius, 0);
        Amg::MatrixX errorMatrix{1, 1};
        errorMatrix(0, 0) = (errRadius * errRadius);

        IdentifierHash mdtHashId;

        // check whether the collection exists; if not, add it to the container
        // each collection has hits from a single chamber.
        elementId = m_idHelperSvc->mdtIdHelper().elementID(StationName, StationEta, StationPhi);
        if (elementId != previousId) {
	    if (mdtCollection) {
	        mdt_chambers++;
		mdt_hits += mdtCollection->size();
                IdentifierHash mdtHashId;
                m_idHelperSvc->mdtIdHelper().get_module_hash(mdtCollection->identify(), mdtHashId);
                if (m_container->addCollection(mdtCollection, mdtHashId) != StatusCode::SUCCESS) {
                    ATH_MSG_WARNING("Could not record MDT PRD Collection of " << mdtCollection->size() << " hits for chamber "
                                                                              << m_idHelperSvc->mdtIdHelper().stationNameString(StationName) << "_"
                                                                              << StationPhi << "_" << StationEta << " in StoreGate!");
                    // return StatusCode::FAILURE;
                }
            }

            previousId = elementId;
            IdContext mdtContext = m_idHelperSvc->mdtIdHelper().module_context();
            if (m_idHelperSvc->mdtIdHelper().get_hash(elementId, mdtHashId, &mdtContext)) {
                ATH_MSG_WARNING("Unable to get MDT hash id from MDT PRD collection "
                                << "context begin_index = " << mdtContext.begin_index()
                                << " context end_index  = " << mdtContext.end_index() << " the identifier is ");
                elementId.show();
                return StatusCode::FAILURE;
            }
            mdtCollection = new Muon::MdtPrepDataCollection(mdtHashId);
            mdtCollection->setIdentifier(elementId);
        }

	// make an MdtPrepData object for this hit
        Muon::MdtPrepData *newPrepData =
            new Muon::MdtPrepData(channelId, mdtHashId, driftRadius, std::move(errorMatrix), detEl, tdc_counts, adc_counts, digitStatus);

        // add the MdtPrepData to the collection
        mdtCollection->push_back(newPrepData);

    }  // end loop over tubes

    // store last collection
    if (mdtCollection) {
        IdentifierHash mdtHashId;
        m_idHelperSvc->mdtIdHelper().get_module_hash(mdtCollection->identify(), mdtHashId);

        mdt_chambers++;
        mdt_hits += mdtCollection->size();
        if (m_container->addCollection(mdtCollection, mdtHashId) != StatusCode::SUCCESS) {
            ATH_MSG_WARNING("Could not record MDT PRD Collection of " << mdtCollection->size() << " hits for chamber "
                                                                      << m_idHelperSvc->mdtIdHelper().stationNameString(StationName) << "_" << StationPhi
                                                                      << "_" << StationEta << " in StoreGate!");
            // return StatusCode::FAILURE;
        }
    }
    ATH_MSG_DEBUG("Wrote " << mdt_hits << " MDT PRD hits for " << mdt_chambers << " chambers");

    return StatusCode::SUCCESS;
}  // MdtPRDContCalibStreamCnv::fillCollections()
