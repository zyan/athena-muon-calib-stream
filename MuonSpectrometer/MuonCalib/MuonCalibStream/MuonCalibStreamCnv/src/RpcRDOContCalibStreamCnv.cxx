#include "MuonCalibStreamCnv/RpcRDOContCalibStreamCnv.h"
#include "MuonCalibStreamCnv/MuonCalibStreamAddress.h" 
#include "MuonCalibStreamCnvSvc/MuonCalibStreamDataProviderSvc.h" 
#include "MuCalDecode/CalibEvent.h" 
#include "MuCalDecode/CalibData.h" 
#include "MuCalDecode/CalibUti.h" 

#include "GaudiKernel/StatusCode.h"
#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/IRegistry.h"
#include "AthenaBaseComps/AthCheckMacros.h"
#include "AthenaBaseComps/AthMsgStreamMacros.h"

#include "MuonPrepRawData/MuonPrepDataCollection.h"
#include "MuonReadoutGeometry/RpcReadoutElement.h"
#include "MuonRDO/RpcPadContainer.h"
#include "MuonRDO/RpcPad.h"
#include "MuonRDO/RpcFiredChannel.h"
#include "MuonRDO/RpcCoinMatrix.h"
#include "MuonIdHelpers/RpcIdHelper.h"
// #include "RPCcablingInterface/RpcPadIdHash.h"
// #include "RPCcablingInterface/IRPCcablingServerSvc.h"
#include "AthenaKernel/StorableConversions.h"
#include "AthenaKernel/errorcheck.h"

#include <iostream>
#include <map> 
#include <list> 

using namespace  LVL2_MUON_CALIBRATION;

RpcRDOContCalibStreamCnv::RpcRDOContCalibStreamCnv(ISvcLocator *svcloc) :
  Converter(MuonCalibStream_StorageType, classID(),svcloc),
  AthMessaging(msgSvc(), "RpcRDOContCalibStreamCnv"),
  m_container(nullptr), 
  m_storeGate(nullptr), 
  m_dataProvider(nullptr), 
  m_muonMgr(nullptr), 
  m_idHelperSvc("MuonIdHelperSvc","Muon::MuonIdHelperSvc/MuonIdHelperSvc"), 
  m_padContainer(nullptr),
  m_readKey("RpcCablingCondData") 
{
  ATH_MSG_DEBUG( " constructor " ); 
}

RpcRDOContCalibStreamCnv::~RpcRDOContCalibStreamCnv() {
}

const CLID& RpcRDOContCalibStreamCnv::classID(){
  return ClassID_traits<RpcPadContainer>::ID();
}

StatusCode RpcRDOContCalibStreamCnv::initialize() {
  ATH_MSG_INFO("Initialize RpcRDOContCalibStreamCnv");

  ATH_CHECK( Converter::initialize() ); 

  // Retrieve the RpcIdHelper and detector descriptor manager
  StoreGateSvc *detStore = nullptr;
  ATH_CHECK( serviceLocator()->service("DetectorStore", detStore) );

  ATH_CHECK(detStore->retrieve(m_muonMgr));
  ATH_CHECK(m_readKey.initialize());

        // retrieve also the dataProviderSvc
  //ATH_CHECK(m_dataProvider.retrieve());
  ATH_CHECK(m_idHelperSvc.retrieve());


  // ATH_CHECK( detStore->retrieve(m_rpcIdHelper, "RPCIDHELPER" ) );
  // ATH_CHECK( detStore->retrieve( m_muonMgr ) );
  
  // // NEW way get RPC cablingSvc
  // ATH_CHECK(m_readKey.initialize());
  // // OLD way, cannot work anymore in A22
  // //  const IRPCcablingServerSvc *RpcCabGet = nullptr;
  // //  ATH_CHECK( service("RPCcablingServerSvc", RpcCabGet) );
  // //  ATH_CHECK( RpcCabGet->giveCabling(m_cabling) );

  // retrieve also the dataProviderSvc
  IService *svc = nullptr;
  ATH_CHECK( serviceLocator()->getService("MuonCalibStreamDataProviderSvc", svc) );
                                                                                
  m_dataProvider = dynamic_cast<MuonCalibStreamDataProviderSvc*> (svc);
  if(m_dataProvider == 0 ) {
    ATH_MSG_ERROR( " Cannot cast to MuonCalibStreamDataProviderSvc " );
    return StatusCode::FAILURE;
  }

  //ATH_CHECK(service("StoreGateSvc", m_storeGate)); 

  // initialize the offset (should be moved to job options)
  //  m_rpcOffset=24000;

  // create an empty pad container and record it
  m_padContainer = new RpcPadContainer(600);
  m_padContainer->addRef();
 
  return StatusCode::SUCCESS;
}  //RpcRDOContCalibStreamCnv::initialize()

StatusCode RpcRDOContCalibStreamCnv::createObj(IOpaqueAddress* /*pAddr*/, DataObject *&pObj) {
  if(m_padContainer) {
    m_padContainer->cleanup();
  } else {    
    // should a PadContainer be created??
    return StatusCode::FAILURE;
  }
  //  E. Diehl Remove these lines which cause this WARNING in log files:.
  //   StoreGateSvc WARNING record_impl: you are recording an object
  //         with key RPCPAD, type RpcPadContainer (CLID 4190)
  // register it in StoreGate.
  //  std::string key = "RPCPAD";
  //  ATH_CHECK( m_storeGate->record(m_padContainer,key) );

  ATH_CHECK( fillCollections(pObj) ); 
  pObj = SG::asStorable( m_padContainer );

  return StatusCode::SUCCESS; 
}  //RpcRDOContCalibStreamCnv::createObj()

StatusCode  RpcRDOContCalibStreamCnv::createRep(DataObject* /*pObj*/, IOpaqueAddress*& /*pAddr*/) {
  ATH_MSG_ERROR( " No conversion RpcPrepData to BS" ); 
  return StatusCode::FAILURE; 
}

StatusCode RpcRDOContCalibStreamCnv::fillCollections(DataObject *&pObj)  {
  ATH_MSG_DEBUG("filling RPC collections");
  const CalibEvent *event= m_dataProvider->getEvent(); 

  if( event->rpc() == nullptr ) {
    ATH_MSG_DEBUG("NO RPC hits!");
    return StatusCode::SUCCESS; 
  }

  //Athena22: Need to replace use of RpcPadIdHash and padHashFunction, do not exist anymore
  //  RpcPadIdHash hashF;
 
  //extract the list of RPC pads
  ATH_MSG_DEBUG("extract the list of RPC pads");
  std::list<RpcCalibData> calib_pad = (event->rpc())->data();

  ATH_MSG_DEBUG( "fillCollections: number of PADS= " << (event->rpc())->size() );
  ATH_MSG_DEBUG( "fillCollections: number of matrices= " << calib_pad.size() );

  //std::vector<RpcPad*>* m_rpcpads;
  // Iterate on the readout PADS
  ATH_MSG_DEBUG("Iterate on the readout PADS");
  int max_pad=1;
  for( int j=0; j<max_pad; ++j ) {
    // Identifier elements
    int name = 0;
    int eta = 0;
    int phi = 0;
    int doublet_r = 0;
    int doublet_z = 0;
    int doublet_phi = 0;
    int gas_gap = 0;
    int measures_phi = 0;
    int strip = 0;
    
    unsigned int sector=(event->rpc())->sectorId();
    unsigned int subsys=(event->rpc())->subsystemId();
    unsigned int pad_id=(event->rpc())->padId();
    unsigned int status=(event->rpc())->status();
    unsigned int errorCode=(event->rpc())->error_code();
    if(subsys==0x65) sector=sector+32;
    
    ATH_MSG_DEBUG( "fillCollections: pad no= " << j << " sector= " << sector << " subsys= " << subsys 
		   << " pad id= " << pad_id << " status= " << status << " error code= " << errorCode );

    int side = (sector < 32) ? 0:1;
    int logic_sector = sector%32;
    int key = side*10000+logic_sector*100+pad_id;
	
    // Retrieve the identifier elements from the map
    //    const CablingRPCBase::RDOmap &pad_map = m_cabling->give_RDOs();  //OLD way

    SG::ReadCondHandle<RpcCablingCondData> readHandle{m_readKey, Gaudi::Hive::currentContext()};
    const RpcCablingCondData* m_cabling{*readHandle};

    const RpcCablingCondData::RDOmap &pad_map = m_cabling->give_RDOs();
    RDOindex index = (*pad_map.find(key)).second;
    index.offline_indexes(name, eta, phi, doublet_r, doublet_z, doublet_phi, gas_gap, measures_phi, strip);
      
    // Build the pad offline identifier
    Identifier id = m_idHelperSvc->rpcIdHelper().padID(name,eta,phi,doublet_r,doublet_z,doublet_phi);

    //IdentifierHash padHash;
    //m_rpcIdHelper->get_detectorElement_hash(id, padHash);
      
    RpcPad *newpad = new RpcPad(id,index.hash(),pad_id,status,errorCode,sector);

    //iterate on the matrices
    ATH_MSG_DEBUG("Iterate on the matrices");
    std::list<RpcCalibData>::const_iterator calib_matrix;
    std::list<RpcCalibData>::const_iterator calib_matrix_begin = calib_pad.begin();
    std::list<RpcCalibData>::const_iterator calib_matrix_end = calib_pad.end();
    for( calib_matrix=calib_matrix_begin; calib_matrix!=calib_matrix_end; ++calib_matrix ) {
      unsigned int cmid=calib_matrix->onlineId();
      unsigned int crc=calib_matrix->crc();
      unsigned int fel1id=calib_matrix->fel1Id();
      unsigned int febcid=calib_matrix->febcId();
      ATH_MSG_DEBUG( "fillCollections: matrix no= " << cmid << " crc= " << crc << " fel1id= " << fel1id << " febcid= " << febcid );
      RpcCoinMatrix *matrix=new RpcCoinMatrix(id,cmid,crc,fel1id,febcid);
      //iterate on the fired channels
      ATH_MSG_DEBUG("Iterate on the fired channels");
      for( short unsigned int i=0; i<calib_matrix->hitNum(); ++i ) {
	RpcFiredChannel *rpcfiredchannel = 0;
	uint16_t bcid=0;
	uint16_t time=0;
	uint16_t ijk=0;
	uint16_t channel=0;
	uint16_t ovl=0;
	uint16_t thr=0;
	calib_matrix->giveHit(i,bcid,time,ijk,channel,ovl,thr); 
	if( ijk < 7 ) {
	  rpcfiredchannel = new RpcFiredChannel(bcid,time,ijk,channel);
	} else if( ijk == 7 ) {
	  rpcfiredchannel = new RpcFiredChannel(bcid,time,ijk,thr,ovl);
	}
	ATH_MSG_DEBUG( "fillCollections: hit no= " << i << " bcid= " << bcid << " time= " << time << " ijk= " << ijk
		       << " channel=" << channel << " ovl= " << ovl << " thr= " << thr );
	matrix->push_back(rpcfiredchannel);
	ATH_MSG_DEBUG("fillCollections: hit added to the matrix");
      }  //end iterate on the fired channels
      //add the matrix to the pad
      newpad->push_back(matrix); 
      ATH_MSG_DEBUG("fillCollections: matrix added to the pad");
    }  //end iterate on the matrices

    // Push back the decoded pad in the vector
    Identifier padId = newpad->identify();
    IdentifierHash elementHash;
    m_idHelperSvc->rpcIdHelper().get_detectorElement_hash(padId, elementHash);

    // RpcPadIdHash padHashFunction = *(m_cabling->padHashFunction()); 
    // IdentifierHash elementHash = padHashFunction(padId);

    //int elementHash = hashF( padId );  
    //m_rpcIdHelper->get_detectorElement_hash(padId, elementHash);
    ATH_CHECK( m_padContainer->addCollection(newpad,elementHash ) );

    //      m_rpcpads->push_back(newpad); 
    ATH_MSG_DEBUG("fillCollections: pad added to the pad vector");
  }  //end iterate on the readout PADS
  
  pObj= m_padContainer;
  ATH_MSG_DEBUG("End fill Collection" );
  return StatusCode::SUCCESS; 
}  //RpcRDOContCalibStreamCnv::fillCollections()
