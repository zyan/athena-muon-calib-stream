#include "MuonCalibStreamCnv/MuonCalibStreamTestAlg.h"

#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/PropertyMgr.h"
#include "GaudiKernel/SmartDataPtr.h"
#include "MuonPrepRawData/MuonPrepDataContainer.h"
#include "MuonRDO/CscRawDataContainer.h"

MuonCalibStreamTestAlg::MuonCalibStreamTestAlg(const std::string &name, ISvcLocator *pSvcLocator) :
    //AthAlgorithm(name, pSvcLocator), m_activeStore(nullptr), m_mdtIdHelper(nullptr) {
    AthAlgorithm(name, pSvcLocator), m_activeStore(nullptr) {
    m_byCont = 1;
    m_byColl = 0;
    declareProperty("ByCont", m_byCont);
    declareProperty("ByColl", m_byColl);
}

MuonCalibStreamTestAlg::~MuonCalibStreamTestAlg() = default;
StatusCode MuonCalibStreamTestAlg::initialize() {
    ATH_MSG_INFO("initialize");

    // retrieve the active store
    ATH_CHECK(serviceLocator()->service("ActiveStoreSvc", m_activeStore));
    //ATH_CHECK(m_MdtPrepData.initialize())
    // Initialize the IdHelper
    //ATH_CHECK(detStore()->retrieve(m_mdtIdHelper, "MDTIDHELPER"));

    return StatusCode::SUCCESS;
}

//  Dumps MDT and RPC data; CSC, TGC not so much because code unfinished
StatusCode MuonCalibStreamTestAlg::execute() {
    ATH_MSG_DEBUG("execute");

    // std::string csckey = "CSCRDO";
    // const CscRawDataContainer *cscRdoCont;

    // ATH_CHECK(evtStore()->retrieve(cscRdoCont, csckey));
    // ATH_MSG_INFO("Retrieved CscRawDataContainer");
    // Dumping of CSC info never finished
    /*
      CscRawDataContainer::const_iterator cscCollItr = cscRdoCont->begin();
      CscRawDataContainer::const_iterator cscCollEnd = cscRdoCont->end();
      for(; cscCollItr != cscCollEnd; cscCollItr++)
    */
    //SG::ReadHandle<Muon::MdtPrepDataContainer> mdtPrds(m_MdtPrepData, ctx);
    std::string key = "MDT_DriftCircles";
    const Muon::MdtPrepDataContainer *mdtPrds;
    ATH_CHECK(evtStore()->retrieve(mdtPrds, key));
    ATH_MSG_DEBUG("****** MDT Container size: " << mdtPrds->size());

    // If dumping by container
    if (m_byCont != 0) {
        Muon::MdtPrepDataContainer::const_iterator it = mdtPrds->begin();
        Muon::MdtPrepDataContainer::const_iterator it_end = mdtPrds->end();

        for (; it != it_end; ++it) {
            ATH_MSG_DEBUG("MDT ********** Collections size: " << (*it)->size());
            Muon::MdtPrepDataCollection::const_iterator cit_begin = (*it)->begin();
            Muon::MdtPrepDataCollection::const_iterator cit_end = (*it)->end();
            Muon::MdtPrepDataCollection::const_iterator cit = cit_begin;
            for (; cit != cit_end; ++cit) {
                const Muon::MdtPrepData *mdt = (*cit);
                // Identifier id = mdt->identify();
                mdt->dump(msg());
            }
        }
    }
    // If dumping by collection
    if (m_byColl != 0) {
        const DataHandle<Muon::MdtPrepDataCollection> mdtCollection;
        const DataHandle<Muon::MdtPrepDataCollection> lastColl;

        ATH_CHECK(evtStore()->retrieve(mdtCollection, lastColl));
        ATH_MSG_DEBUG("MDT collections retrieved");
        for (; mdtCollection != lastColl; ++mdtCollection) {
            Muon::MdtPrepDataCollection::const_iterator cit_begin = (mdtCollection)->begin();
            Muon::MdtPrepDataCollection::const_iterator cit_end = (mdtCollection)->end();
            Muon::MdtPrepDataCollection::const_iterator cit = cit_begin;
            for (; cit != cit_end; ++cit) {  // first
                const Muon::MdtPrepData *mdt = (*cit);
                mdt->dump(msg());
            }
        }
    }

    std::string key2 = "RPC_Measurements";
    const Muon::RpcPrepDataContainer *rpcPrds;
    ATH_CHECK(evtStore()->retrieve(rpcPrds, key2));
    ATH_MSG_DEBUG("****** RPC Container size: " << rpcPrds->size());
    // If dumping by container
    if (m_byCont != 0) {
        Muon::RpcPrepDataContainer::const_iterator it = rpcPrds->begin();
        Muon::RpcPrepDataContainer::const_iterator it_end = rpcPrds->end();

        for (; it != it_end; ++it) {
            ATH_MSG_DEBUG("********** RPC Collections size: " << (*it)->size());
            Muon::RpcPrepDataCollection::const_iterator cit_begin = (*it)->begin();
            Muon::RpcPrepDataCollection::const_iterator cit_end = (*it)->end();
            Muon::RpcPrepDataCollection::const_iterator cit = cit_begin;
            for (; cit != cit_end; ++cit) {
                const Muon::RpcPrepData *rpc = (*cit);
                rpc->dump(msg());
            }
        }
    }

    // If dumping by collection
    if (m_byColl != 0) {
        const DataHandle<Muon::RpcPrepDataCollection> rpcCollection;
        const DataHandle<Muon::RpcPrepDataCollection> lastColl;

        ATH_CHECK(evtStore()->retrieve(rpcCollection, lastColl));
        ATH_MSG_DEBUG("RPC collections retrieved");
        for (; rpcCollection != lastColl; ++rpcCollection) {
            Muon::RpcPrepDataCollection::const_iterator cit_begin = (rpcCollection)->begin();
            Muon::RpcPrepDataCollection::const_iterator cit_end = (rpcCollection)->end();
            Muon::RpcPrepDataCollection::const_iterator cit = cit_begin;
            for (; cit != cit_end; ++cit) {  // first
                const Muon::RpcPrepData *rpc = (*cit);
                rpc->dump(msg());
            }
        }
    }

    // add the TGC_Measurements
    std::string key3 = "TGC_Measurements";
    const Muon::TgcPrepDataContainer *tgcPrds;
    ATH_CHECK(evtStore()->retrieve(tgcPrds, key3));
    ATH_MSG_DEBUG("****** TGC Container size: " << tgcPrds->size());
    // If dumping by container
    if (m_byCont != 0) {
        Muon::TgcPrepDataContainer::const_iterator it = tgcPrds->begin();
        Muon::TgcPrepDataContainer::const_iterator it_end = tgcPrds->end();

        for (; it != it_end; ++it) {
            ATH_MSG_DEBUG("********** TGC Collections size: " << (*it)->size());
            Muon::TgcPrepDataCollection::const_iterator cit_begin = (*it)->begin();
            Muon::TgcPrepDataCollection::const_iterator cit_end = (*it)->end();
            Muon::TgcPrepDataCollection::const_iterator cit = cit_begin;
            for (; cit != cit_end; ++cit) {
                const Muon::TgcPrepData *tgc = (*cit);
                tgc->dump(msg());
            }
        }
    }



    return StatusCode::SUCCESS;
}  // MuonCalibStreamTestAlg::execute()

StatusCode MuonCalibStreamTestAlg::finalize() { return StatusCode::SUCCESS; }
