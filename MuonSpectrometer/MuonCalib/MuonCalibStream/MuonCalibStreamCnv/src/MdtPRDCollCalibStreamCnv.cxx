/*
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
*/
#include "MuonCalibStreamCnv/MdtPRDCollCalibStreamCnv.h"

#include "AthenaBaseComps/AthCheckMacros.h"
#include "AthenaBaseComps/AthMsgStreamMacros.h"
#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/IRegistry.h"
#include "MuCalDecode/CalibData.h"
#include "MuCalDecode/CalibEvent.h"
#include "MuCalDecode/CalibUti.h"
#include "MuonCalibEvent/MdtCalibHit.h"
#include "MuonCalibStreamCnvSvc/MuonCalibStreamAddress.h"
#include "MuonPrepRawData/MdtPrepDataCollection.h"
#include "MuonReadoutGeometry/MdtReadoutElement.h"

//#include "MuonDigitContainer/MdtIdHash.h"

#include <iostream>
#include <list>
#include <map>

#include "StoreGate/StoreGateSvc.h"

using namespace LVL2_MUON_CALIBRATION;

MdtPRDCollCalibStreamCnv::MdtPRDCollCalibStreamCnv(ISvcLocator *svcloc) :
    AthConstConverter(storageType(), classID(), svcloc, "MdtPRDCollCalibStreamCnv") {
    ATH_MSG_DEBUG(" construct ");
}

const CLID &MdtPRDCollCalibStreamCnv::classID() { return ClassID_traits<Muon::MdtPrepDataCollection>::ID(); }

StatusCode MdtPRDCollCalibStreamCnv::initialize() {
    ATH_MSG_DEBUG(" initialize ");
    // Retrieve the MdtIdHelper and get the detector descriptor manager
    StoreGateSvc *detStore = nullptr;
    ATH_CHECK(serviceLocator()->service("DetectorStore", detStore));
    ATH_CHECK(detStore->retrieve(m_mdtIdHelper, "MDTIDHELPER"));
    ATH_CHECK(detStore->retrieve(m_muonMgr));

    // retrieve the dataProviderSvc
    ATH_CHECK(m_dataProvider.retrieve());
    ATH_CHECK(m_calib_tool.retrieve());

    // get MDT Calibration service
    // ATH_CHECK( serviceLocator()->service("MdtCalibrationSvc", m_calib_tool) );
    return StatusCode::SUCCESS;
}

StatusCode MdtPRDCollCalibStreamCnv::createObjConst(IOpaqueAddress *pAddr, DataObject *&pObj) const {
    MuonCalibStreamAddress *pRE_Addr{nullptr};
    pRE_Addr = dynamic_cast<MuonCalibStreamAddress *>(pAddr);
    if (!pRE_Addr) {
        ATH_MSG_ERROR(" Cannot cast to MuonCalibStreamAddress ");
        return StatusCode::FAILURE;
    }

    const std::string &nm = *(pRE_Addr->par());
    ATH_MSG_DEBUG(" Creating Objects   " << nm);

    Muon::MdtPrepDataCollection *coll;
    ATH_CHECK(fillCollections(coll, pRE_Addr->ipar()));
    pObj = StoreGateSvc::asStorable(coll);

    return StatusCode::SUCCESS;
}  // MdtPRDCollCalibStreamCnv::createObj()

StatusCode MdtPRDCollCalibStreamCnv::createRepConst(DataObject * /*pObj*/, IOpaqueAddress *& /*pAddr*/) const {
    ATH_MSG_ERROR(" No conversion MdtPrepData to BS ");
    return StatusCode::FAILURE;
}

StatusCode MdtPRDCollCalibStreamCnv::fillCollections(Muon::MdtPrepDataCollection *&mdtCollection, const unsigned long *ipar) const {
    /*
      MdtIdHash hashFunc;
      Muon::MdtPrepDataCollection::ID collId = hashFunc.identifier(ipar[0]);
      mdtCollection = new Muon::MdtPrepDataCollection();
      mdtCollection->setIdentifier(collId);
    */
    mdtCollection = new Muon::MdtPrepDataCollection(ipar[0]);

    const CalibEvent *event = m_dataProvider->getEvent();

    // extract the list of MDT hit tubes
    // loop over MDT hit tubes
    // assumes only leading tdc_counts is present on the data
    for (MdtCalibData &tube : event->mdt()->data()) {
        uint16_t coarse = tube.leadingCoarse();
        uint16_t fine = tube.leadingFine();
        int tdc_counts = fine | (coarse << 5);
        int StationName{0}, StationEta{0}, StationPhi{0}, MultiLayer{0}, TubeLayer{0}, Tube{0};
        WordIntoMdtId(tube.id(), StationName, StationEta, StationPhi, MultiLayer, TubeLayer, Tube);

        // convert the above info to a MdtPrepData object
        Identifier elementId = m_mdtIdHelper->elementID(StationName, StationEta, StationPhi);
        if (elementId == mdtCollection->identify()) {
            Muon::MdtDriftCircleStatus digitStatus = Muon::MdtStatusDriftTime;
            Identifier channelId = m_mdtIdHelper->channelID(StationName, StationEta, StationPhi, MultiLayer, TubeLayer, Tube);
            ATH_MSG_DEBUG(channelId << " " << tdc_counts << " " << tube.width() << " " << tube.phi() << " "
                                    << m_mdtIdHelper->stationNameString(StationName) << "_" << StationPhi << "_" << StationEta << "-"
                                    << MultiLayer << "-" << TubeLayer << "-" << Tube);

            double radius{0.}, errRadius{0.};
            const MuonGM::MdtReadoutElement *descriptor = m_muonMgr->getMdtReadoutElement(channelId);
            Amg::Vector3D position = descriptor->tubePos(channelId);
            double measured_perp = position.perp();
            if (descriptor->getStationS() != 0.) { measured_perp = std::hypot(measured_perp, descriptor->getStationS()); }
            const double measured_x = measured_perp * std::cos(position.phi());
            const double measured_y = measured_perp * std::sin(position.phi());
            const Amg::Vector3D measured_position(measured_x, measured_y, position.z());
            MdtCalibHit calib_hit = MdtCalibHit(channelId, tdc_counts, tube.width(), measured_position, descriptor);
            double signedTrackLength = measured_position.mag();

            if (!m_calib_tool->driftRadiusFromTime(calib_hit, signedTrackLength)) return StatusCode::FAILURE;
            radius = calib_hit.driftRadius();
            errRadius = calib_hit.sigmaDriftRadius();

            const Amg::Vector2D driftRadius(radius, 0.);
            Amg::MatrixX errorMatrix(1, 1);
            errorMatrix.setIdentity();
            errorMatrix *= errRadius * errRadius;

            IdentifierHash mdtHashId;  // should be set properly, Domizia
            Muon::MdtPrepData *newPrepData = new Muon::MdtPrepData(channelId, mdtHashId, driftRadius, std::move(errorMatrix), descriptor,
                                                                   tdc_counts, tube.width(), digitStatus);

            // add the MdtPrepData to the collection
            mdtCollection->push_back(newPrepData);
        }
    }

    return StatusCode::SUCCESS;
}  // MdtPRDCollCalibStreamCnv::fillCollections()
