#See: https://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/SoftwareTutorialxAODAnalysisInCMake for more details about anything here

#testFile = '/afs/atlas.umich.edu/home/zyan/workspace/data18_13TeV.00363979.physics_Main.merge.DESDM_MCP.f1002_m2041._0993.1'
testFile = '/afs/atlas.umich.edu/home/zyan/workspace/ReprocessedDESD/DESDM_MCP.26844994._000003.pool.root.1'
#testFile = os.getenv("ALRB_TutorialData") + '/mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_PHYS.e6337_s3126_r10201_p4172/DAOD_PHYS.21569875._001323.pool.root.1'

#override next line on command line with: --filesInput=XXX
jps.AthenaCommonFlags.FilesInput = [testFile] 

#Specify AccessMode (read mode) ... ClassAccess is good default for xAOD
#What's the good default mode for DESD ?
jps.AthenaCommonFlags.AccessMode = "ClassAccess" 


# Create the algorithm's configuration.
from AnaAlgorithm.DualUseConfig import createAlgorithm
alg = createAlgorithm ( 'MuonSegmentReader','AnalysisAlg' )
#alg = createAlgorithm ( 'MyxAODAnalysis', 'AnalysisAlg' )
# later on we'll add some configuration options for our algorithm that go here#

# Add our algorithm to the main alg sequence
athAlgSeq += alg

# limit the number of events (for testing purposes)
theApp.EvtMax = 10

# optional include for reducing printout from athena
include("AthAnalysisBaseComps/SuppressLogging.py")
