#athenaCommonFlags.EvtMax = -1
# setup trigger config
from RecExConfig.RecFlags import rec
#rec.doTrigger = False
from RecExConfig.RecAlgsFlags import recAlgs
#recAlgs.doTrigger = False
#TriggerFlags.triggerConfig = "OFF"

rec.doWriteAOD = False
rec.doWriteESD = False
rec.doWriteTAG = False
rec.doAOD = False
rec.doDPD = False
rec.doESD = False

# switch off as much as possible
rec.doTruth = False
rec.doRecoTiming = False
rec.doShowSizeStatistics = False
rec.readTAG = False
rec.readRDO = False
rec.doHist = False
rec.doContainerRemapping = False
rec.doJiveXML = False
rec.doEdmMonitor = False
rec.doDumpPoolInputContent = False
rec.doHeavyIon = False
rec.doHIP = False
rec.doWriteBS = False
rec.doPhysValMonHists = False
rec.doVP1 = False
rec.doJiveXML = False
rec.doCheckDictionary = False
rec.doFileMetaData = False
rec.doAODCaloCells = False
recAlgs.doAtlfast = False
recAlgs.doMonteCarloReact = False

rec.doEgamma = False
rec.CBNTAthenaAware = False
#rec.doAODSelect = False
rec.doWritexAOD = False
rec.doPerfMon = False
rec.doTagRawSummary = False
rec.doBTagging = False
rec.doSemiDetailedPerfMon = False
rec.doAODall = False
rec.doTau = False
rec.doJetMissingETTag = False

recAlgs.doCaloTrkMuId = False
recAlgs.doEFlow = False
recAlgs.doMissingET = False
recAlgs.doMuGirl = False
recAlgs.doMuTag = False
recAlgs.doMuonIDCombined = False
recAlgs.doMuonIDStandAlone = False
recAlgs.doStaco = False
recAlgs.doTileMuID = False
recAlgs.doTrackParticleCellAssociation = False
recAlgs.doTrackRecordFilter = False

#at the moment, in case you try to include RecExCommon with an empty InputCollection, it will search for an default file which is not there
#-> remove the include for the grid submission
if len(athenaCommonFlags.FilesInput.get_Value()) > 0 or len(ServiceMgr.EventSelector.InputCollections) > 0:
    include("RecExCommon/RecExCommon_topOptions.py")
    # also do not do the following on the grid
    rec.AutoConfiguration = ["everything"]
# do not spam the grid
else:
    ServiceMgr.MessageSvc.verboseLimit = 1
    ServiceMgr.MessageSvc.warningLimit = 1
    ServiceMgr.MessageSvc.infoLimit = 1

### Setup the muon cabling services
from MuonCnvExample import MuonCablingConfig
if not hasattr(svcMgr, 'xAODConfigSvc'):
    from TrigConfxAOD.TrigConfxAODConf import TrigConf__xAODConfigSvc
    svcMgr += TrigConf__xAODConfigSvc('xAODConfigSvc')

svcMgr.xAODConfigSvc.UseInFileMetadata = False
