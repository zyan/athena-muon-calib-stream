###################################################
# job options file to run the CalibSegmentCreator     #
###################################################

from AthenaCommon.AppMgr import ServiceMgr
from AthenaCommon.AthenaCommonFlags import athenaCommonFlags

# import os, sys, logging
# if os.getenv("AtlasProject") in ["Athena", "AthDerivation"]:
#     import AthenaPoolCnvSvc.ReadAthenaPool
# else:
#     import AthenaRootComps.ReadAthenaxAOD
#     ServiceMgr.EventSelector.AccessMode = 1  #0 = branch, 1 = class, 2 = 'athena', -1 = use TEvent's default
# isMaster = False
# if os.getenv("AtlasVersion").startswith("22"): isMaster = True

#--------------------------------------------------------------
# Configure algorithm.
#--------------------------------------------------------------
include("setupRecExCommon.py")
#include("MuonCalibSegmentCreator/setupRecExCommon.py")
from AthenaCommon.DetFlags import DetFlags

DetFlags.detdescr.all_setOn()


# Full job is a list of algorithms
from AthenaCommon.AlgSequence import AlgSequence

job = AlgSequence()

from AnaAlgorithm.DualUseConfig import createAlgorithm
alg = createAlgorithm ( 'MuonCalib::MuonSegmentReader','AnalysisAlg' )
#alg = createAlgorithm ( 'MyxAODAnalysis', 'AnalysisAlg' )
# later on we'll add some configuration options for our algorithm that go here#

# Add our algorithm to the main alg sequence
job += alg

# setup output histogram
svcMgr += CfgMgr.THistSvc()
svcMgr.THistSvc.Output += ["CALIBNTUPLESTREAM DATAFILE='ntuple.root' OPT='RECREATE'"]
#svcMgr.THistSvc.Output += ["ANOTHERSTREAM DATAFILE='anotherfile.root' OPT='RECREATE'"]

# limit the number of events (for testing purposes)
theApp.EvtMax = 10

# optional include for reducing printout from athena
include("AthAnalysisBaseComps/SuppressLogging.py")
