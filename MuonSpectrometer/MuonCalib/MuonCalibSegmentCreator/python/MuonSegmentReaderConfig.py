# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration

# from os import access
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory

def MuonSegmentReaderCfg(configFlags, **kwargs):
    from MuonConfig.MuonReconstructionConfig import MuonReconstructionCfg

    #setup the tools
    from MuonConfig.MuonGeometryConfig import MuonIdHelperSvcCfg
    from MuonConfig.MuonRecToolsConfig import MuonEDMHelperSvcCfg


    result=ComponentAccumulator()
    result.merge(MuonIdHelperSvcCfg(configFlags))
    result.merge(MuonEDMHelperSvcCfg(configFlags))
    result.merge(MuonReconstructionCfg(configFlags))

    alg = CompFactory.MuonCalib.MuonSegmentReader(**kwargs)


    result.addEventAlgo(alg)
    #result.merge(alg)
    return result

# if __name__=="__main__":
#     from AthenaCommon.Configurable import Configurable
#     from AthenaConfiguration.AllConfigFlags import ConfigFlags
#     from AthenaConfiguration.MainServicesConfig import MainServicesCfg

#     Configurable.configurableRun3Behavior=1

#     #from AthenaConfiguration.TestDefaults import defaultTestFiles
#     #ConfigFlags.Input.Files = defaultTestFiles.ESD
#     ConfigFlags.Input.Files = '/afs/atlas.umich.edu/home/zyan/workspace/ReprocessedDESD/DESDM_MCP.26844994._000003.pool.root.1'
#     #ConfigFlags.Input.Files = '/afs/atlas.umich.edu/home/zyan/workspace/data18_13TeV.00358615.physics_Main.merge.DESDM_MCP.f961_m2024._0100.1'
#     ConfigFlags.lock()

#     cfg = MainServicesCfg(ConfigFlags)
    
#     acc=MuonSegmentReaderCfg(ConfigFlags)
#     cfg.merge(acc)

#     cfg.run(3)

if __name__ == "__main__":
    from AthenaCommon.Configurable import Configurable
    Configurable.configurableRun3Behavior = 1

    # import the flags and set them
    from AthenaConfiguration.AllConfigFlags import ConfigFlags
    
    ConfigFlags.Exec.MaxEvents = 3
    
    # use one of the predefined files
    from AthenaConfiguration.TestDefaults import defaultTestFiles
    ConfigFlags.Input.Files = defaultTestFiles.RAW

    # lock the flags
    ConfigFlags.lock()

    # create basic infrastructure
    from AthenaConfiguration.MainServicesConfig import MainServicesCfg
    acc = MainServicesCfg(ConfigFlags)
    histSvc = CompFactory.THistSvc(Output=["%s DATAFILE='%s', OPT='RECREATE'" % ("CALIBNTUPLESTREAM", 'ntuple.root')])
    acc.addService(histSvc, primary=True)

    # add the algorithm to the configuration
    #acc.merge(MyAlgCfg(ConfigFlags))
    # from MuonConfig.MuonReconstructionConfig import MuonReconstructionCfg
    # acc.merge(MuonReconstructionCfg(ConfigFlags))
    #log.info("---------- Configured muon tracking")
    alg=MuonSegmentReaderCfg(ConfigFlags)
    acc.merge(alg)

    # debug printout
    acc.printConfig(withDetails=True, summariseProps=True)

    # run the job
    sc = acc.run()

    # report the execution status (0 ok, else error)
    import sys
    sys.exit(not sc.isSuccess())
