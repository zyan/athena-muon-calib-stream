/*
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
*/
    
#include "MuonCalibSegmentCreator/MuonSegmentReader.h"
#include "GaudiKernel/ITHistSvc.h"
//#include "MuonRIO_OnTrack/CscClusterOnTrack.h"
#include "MuonRIO_OnTrack/MdtDriftCircleOnTrack.h"
#include "MuonRIO_OnTrack/RpcClusterOnTrack.h"
#include "MuonRIO_OnTrack/TgcClusterOnTrack.h"




namespace MuonCalib {

StatusCode MuonSegmentReader::initialize()
{

  ATH_CHECK(m_evtKey.initialize() );
  ATH_CHECK(m_TrkSegKey.initialize());
  ATH_CHECK(m_TrkKey.initialize());
  ATH_CHECK(m_MdtPrepData.initialize());
  ATH_CHECK(m_calibrationTool.retrieve());
  ATH_MSG_VERBOSE("MdtCalibrationTool retrieved with pointer = " << m_calibrationTool);

  ATH_CHECK(m_DetectorManagerKey.initialize());
  ATH_CHECK(m_idToFixedIdTool.retrieve());
  ATH_CHECK(m_pullCalculator.retrieve());
  // ATH_CHECK(m_assocTool.retrieve());
  // ATH_CHECK(m_idHelperSvc.retrieve());
  ATH_CHECK(m_MuonIdHelper.retrieve());
  //ATH_CHECK(histSvc.retrieve() );


  // m_tree->Branch("trk_nTracks", &m_trk_nTracks);
  // m_tree->Branch("trkHit_nHits",&m_trkHit_nHits);
  // m_tree->Branch("trkHit_tdc", &m_trkHit_tdc) ;
  // m_tree->Branch("trkHit_trackIndex", &m_trkHit_trackIndex) ;
  // m_tree->Branch("trkHit_driftRadius", &m_trkHit_driftRadius) ;
  // m_tree->Branch("trkHit_driftTime", &m_trkHit_driftTime) ;
  // m_tree->Branch("trkHit_distRO", &m_trkHit_distRO) ;
  // m_tree->Branch("trkHit_error", &m_trkHit_error) ;
  // m_tree->Branch("trkHit_resi", &m_trkHit_resi) ;
  // m_tree->Branch("trkHit_pull", &m_trkHit_pull) ;

  // m_tree->Branch("seg_nSegments", &m_seg_nSegments);
  // m_tree->Branch("seg_nHits", &m_seg_nHits);
  // m_tree->Branch("seg_nMdtHits", &m_seg_nMdtHits);
  // m_tree->Branch("seg_nRpcHits", &m_seg_nRpcHits);
  // m_tree->Branch("seg_nCscHits", &m_seg_nCscHits);
  // m_tree->Branch("seg_nTgcHits", &m_seg_nTgcHits);

  // m_tree->Branch("mdt_nMdt", &m_mdt_nMdt);
  // m_tree->Branch("mdt_segIndex", &m_mdt_segIndex);
  // m_tree->Branch("mdt_id", &m_mdt_id);
  // m_tree->Branch("mdt_adc", &m_mdt_adc);
  // m_tree->Branch("mdt_tdc", &m_mdt_tdc);
  
  ATH_CHECK(m_tree.init(this));


  // stree->Branch("seg_muon_n", &seg_muon_n);
  // stree->Branch("seg_author", &seg_author);
  // stree->Branch("seg_eta_index", &seg_eta_index);
  // stree->Branch("seg_tech", &seg_tech);
  // stree->Branch("seg_cham_index", &seg_cham_index);
  // stree->Branch("seg_name", &seg_name);
  // stree->Branch("seg_name_short", &seg_name_short);
  // stree->Branch("seg_haveTrkSeg", &seg_haveTrkSeg);
  // stree->Branch("seg_identifier", &seg_identifier);
  // stree->Branch("seg_index", &seg_index);
  // stree->Branch("seg_x", &seg_x);
  // stree->Branch("seg_y", &seg_y);
  // stree->Branch("seg_z", &seg_z);
  // stree->Branch("seg_px", &seg_px);
  // stree->Branch("seg_py", &seg_py);
  // stree->Branch("seg_pz", &seg_pz);
  // stree->Branch("seg_t0", &seg_t0);
  // stree->Branch("seg_t0_error", &seg_t0_error);
  // stree->Branch("seg_chisquare", &seg_chisquare);
  // stree->Branch("seg_nDOF", &seg_nDOF);
  // stree->Branch("seg_nPrecisionHits", &seg_nPrecisionHits);
  // stree->Branch("seg_nTrigEtaLayers", &seg_nTrigEtaLayers);
  // stree->Branch("seg_nTrigPhiLayers", &seg_nTrigPhiLayers);
  // stree->Branch("seg_global_x", &seg_global_x);
  // stree->Branch("seg_global_y", &seg_global_y);
  // stree->Branch("seg_global_z", &seg_global_z);
  // stree->Branch("seg_global_px", &seg_global_px);
  // stree->Branch("seg_global_py", &seg_global_py);
  // stree->Branch("seg_global_pz", &seg_global_pz);
  // stree->Branch("seg_local_x", &seg_local_x);
  // stree->Branch("seg_local_y", &seg_local_y);
  // stree->Branch("seg_local_z", &seg_local_z);
  // stree->Branch("seg_local_px", &seg_local_px);
  // stree->Branch("seg_local_py", &seg_local_py);
  // stree->Branch("seg_local_pz", &seg_local_pz);
  // stree->Branch("seg_cham_x", &seg_cham_x);
  // stree->Branch("seg_cham_y", &seg_cham_y);
  // stree->Branch("seg_cham_z", &seg_cham_z);
  // stree->Branch("seg_vMB_x", &seg_vMB_x);
  // stree->Branch("seg_vMB_y", &seg_vMB_y);
  // stree->Branch("seg_vMB_z", &seg_vMB_z);
  // stree->Branch("seg_nhits", &seg_nhits);
  // stree->Branch("seg_ntrackhits", &seg_ntrackhits);
  // stree->Branch("seg_track_hitshare", &seg_track_hitshare);
  // stree->Branch("seg_hitshare", &seg_hitshare);
  // stree->Branch("seg_hitreq", &seg_hitreq);
  // stree->Branch("seg_fitqual", &seg_fitqual);

  
  return StatusCode::SUCCESS;
}

StatusCode MuonSegmentReader::execute()
{

  // eventInfo
  //const xAOD::EventInfo* eventInfo = 0;
  const EventContext& ctx = Gaudi::Hive::currentContext();
  SG::ReadHandle<xAOD::EventInfo> eventInfo(m_evtKey,ctx);
  if (!eventInfo.isValid()) {
    ATH_MSG_ERROR("Did not find xAOD::EventInfo");
    return StatusCode::FAILURE;
  }


  SG::ReadHandle<TrackCollection> muTrks(m_TrkKey, ctx);
  ATH_MSG_INFO("Number of Muon StandAlone Tracks : "<<muTrks->size());

  //if(!muTrks->size()) continue;  

  m_trk_nTracks = muTrks->size() ;
  //m_nEvent   = eventInfo->nEvent();

  m_runNumber   = eventInfo->runNumber();
  m_eventNumber = eventInfo->eventNumber();
  m_lumiBlock    = eventInfo->lumiBlock();
  m_bcId        = eventInfo->bcid();
  m_timeStamp = eventInfo->timeStamp();
  //m_eventTag = eventInfo->eventTag();
  

  // if tracks were found, print MDT track hits
  for (unsigned itrk = 0; itrk < muTrks->size(); ++itrk) {
    
    const Trk::Track* trkSA = muTrks->at(itrk) ;
    const DataVector<const Trk::TrackStateOnSurface>* trk_states = trkSA->trackStateOnSurfaces();
    const DataVector<const Trk::MeasurementBase>* trk_mT = trkSA->measurementsOnTrack();

    int ntm(0), ntr(0), ntt(0), ntc(0),ntmm(0), nts(0);
    int count = 0;
    for (const Trk::TrackStateOnSurface* trk_state : *trk_states) {
        const Trk::MeasurementBase* it = trk_state->measurementOnTrack();

        if (!it) {
            ATH_MSG_DEBUG("No Measurement!");
            continue;
        }

        const Trk::RIO_OnTrack* rot = dynamic_cast<const Trk::RIO_OnTrack*>(it);
        if (!rot) {
            ATH_MSG_DEBUG("No ROT!");
            // use competingRIO from RPC and TGC

            continue;
        }

        Amg::Vector3D mb_pos = it->globalPosition();
        double error = std::sqrt(rot->localCovariance()(0,0)); 
        ATH_MSG_INFO("track measurementbase hit global position measurement " << mb_pos<<" error "<<error);
        const Trk::TrackParameters *trackPars = trk_state->trackParameters();
       

          if (rot->identify() != 0){
            Identifier rot_id = rot->identify();
            if (m_MuonIdHelper->isMdt(rot_id))  {
              //ATH_MSG_INFO("MDT Track Hit, ID "<<rot_id);
              ntm++;
              const Muon::MdtDriftCircleOnTrack* mrot = dynamic_cast<const Muon::MdtDriftCircleOnTrack*>(rot);
                  if (!mrot) {
                      ATH_MSG_INFO("This is not a  MdtDriftCircleOnTrack!!! ");
                      continue;
                  }
                  //      mdtSegment = true;

                  // get digit from segment
                  const Muon::MdtPrepData* prd = mrot->prepRawData();

                  // digit identifier
                  Identifier id = prd->identify();
                  int adc = prd->adc();
                  int tdc = prd->tdc();
                  m_trkHit_adc.push_back(adc) ;
                  m_trkHit_tdc.push_back(tdc) ;
                  m_trkHit_trackIndex.push_back(itrk) ;
                  m_trkHit_driftRadius.push_back(mrot->driftRadius()) ;
                  m_trkHit_driftTime.push_back(mrot->driftTime()) ;
                  m_trkHit_distRO.push_back(mrot->positionAlongWire()) ;
                  m_trkHit_error.push_back(error) ;

                  Amg::Vector3D mrot_pos = mrot->globalPosition();
                  ATH_MSG_INFO("MdtDriftCircleOnTrack hit global position measurement " << mrot_pos);
                  m_trkHit_pos.push_back(mrot_pos);

                  // residual calculator 
                  const Trk::ResidualPull* resPull = 0;
                  double residual = -999.;
                  double pull = -999.;
                  if( trackPars ) {
                      resPull = m_pullCalculator->residualPull( it, trackPars, Trk::ResidualPull::Unbiased );
                      if( resPull ){
                        residual = resPull->residual().front();
                        pull = resPull->pull().front();
                        delete resPull;
                      }
                  ATH_MSG_INFO("residual " << residual << " pull " << pull);
                  m_trkHit_resi.push_back(residual) ;
                  m_trkHit_pull.push_back(pull) ;

            




            //int mdt_segIndex = iseg ;
            
            MuonFixedId fixid = m_idToFixedIdTool->idToFixedId(id);
            m_trkHit_id.push_back(fixid.getIdInt()) ;
            std::string st = fixid.stationNumberToFixedStationString(fixid.stationName());
            int ml = fixid.mdtMultilayer();
            int la = fixid.mdtTubeLayer();
            int tb = fixid.mdtTube();
            ATH_MSG_INFO("TrackIndex "<<itrk<<" station " << st << " eta " << fixid.eta() << " phi " << fixid.phi() << " ML " << ml << " Layer " << la
                                      <<" Tube "<<tb<< " MROT drift R " << mrot->driftRadius() << " drift Time "
                                      << mrot->driftTime() <<" ADC "<<adc<<" TDC "<<tdc);
            //ATH_MSG_INFO("TrackIndex "<<itrk<<" MDT Track Hit, ID "<<id<<" ADC "<<adc<<" TDC "<<tdc);

            } // finish mdt hit loop
      else if (m_MuonIdHelper->isRpc(rot_id)) {
            ATH_MSG_INFO("ROT ID is RPC "<<rot_id);
            ++ntr;
                                          }
      else if (m_MuonIdHelper->isTgc(rot_id)) {
            ATH_MSG_INFO("ROT ID is TGC "<<rot_id);
            ++ntt;
                                          }
      else if (m_MuonIdHelper->isCsc(rot_id)) {
            ATH_MSG_INFO("ROT ID is CsC "<<rot_id);
            ++ntc;
                                          }
      else if (m_MuonIdHelper->isMM(rot_id)) {
                ATH_MSG_INFO("ROT ID is MM "<<rot_id);
                ++ntmm;
                                          }
      else if (m_MuonIdHelper->issTgc(rot_id)) {
                ATH_MSG_INFO("ROT ID is sTgc "<<rot_id);
                ++nts;
                                          }
      else {
            ATH_MSG_INFO("Couldn't find track ROT ID "<<rot_id);
      }
                                          }// end of id tech check
          }// end of id check 
  } // end of track hit loop
  ATH_MSG_INFO("Track "<<itrk<<" Total Track Hits : "<<trk_mT->size()<<", nMDT "<<ntm<<", nRPC "<<ntr<<", nTGC "<<ntt<<", nMM "<<ntmm<<", nsTgc "<<nts<<", nCSC "<<ntc) ;

  } // end of track loop
  m_trkHit_nHits = m_trkHit_adc.size() ;

  // Segment hit branches

  SG::ReadHandle<Trk::SegmentCollection> muTrkSegments(m_TrkSegKey, ctx);
  ATH_MSG_INFO("Number of Muon Segments : "<<muTrkSegments->size());

  m_seg_nSegments = muTrkSegments->size() ;
  // init vector 
  std::vector<int> seg_nMdtHits;
  std::vector<int> seg_nRpcHits;
  std::vector<int> seg_nCscHits;
  std::vector<int> seg_nTgcHits;

  for (unsigned iseg = 0; iseg < muTrkSegments->size(); ++iseg) {
        const Trk::Segment* trk_seg = muTrkSegments->at(iseg);

        // int nOverlapHits = segtrackHitsOverlap(trk_seg, trk);
        // ATH_MSG_INFO("Track "<<itrk<<" Segment "<<iseg<<" OverlapHits "<<nOverlapHits);
        const Muon::MuonSegment* mu_segment = dynamic_cast<const Muon::MuonSegment*>(trk_seg);

        if (!mu_segment) {
            ATH_MSG_DEBUG("MuonSegment invalid");
            continue;
        }

        // // Matching trk segments with muon segments
        // bool is_matched_to_muon = false;
        // double seg_glob_z = mu_segment->globalPosition().z();
        // ATH_MSG_DEBUG("Trk segment z position: " << seg_glob_z);

        // for (const auto associated_segment : all_segments) {
        //     double museg_glob_z = associated_segment->z();
        //     ATH_MSG_DEBUG("muon associated segment z position: " << museg_glob_z);
        //     if (fabs(seg_glob_z - museg_glob_z) < 1e-05) is_matched_to_muon = true;
        // }
        // ATH_MSG_DEBUG("Is matched: " << is_matched_to_muon);
        // m_seg_association += is_matched_to_muon;

        ATH_MSG_INFO("Number of contained ROTs per segment " << mu_segment->numberOfContainedROTs());
        m_seg_nHits.push_back(mu_segment->numberOfContainedROTs()) ;

        int nm(0), nr(0), nt(0), nc(0), nmm(0), ns(0);
        
        for (unsigned irot = 0; irot < mu_segment->numberOfContainedROTs(); ++irot) {
            const Trk::RIO_OnTrack* mu_rot = mu_segment->rioOnTrack(irot);

            if (!mu_rot) {
                ATH_MSG_INFO("No ROT for segment irot:" << irot);
                continue;
            }
            const Amg::MatrixX localCov = mu_rot->localCovariance();
            double error2 = localCov(0, 0);
            // // Find out which technology the roi belongs to
            // double truth_residual = 9999;
            // double truth_res_x = 9999;
            // double truth_res_y = 9999;

            Identifier rot_id = mu_rot->identify();
            if (m_MuonIdHelper->isMdt(rot_id)) {
                //ATH_MSG_INFO("ROT ID is MDT "<<rot_id);
                ++nm;

                const Muon::MdtDriftCircleOnTrack* mrot = dynamic_cast<const Muon::MdtDriftCircleOnTrack*>(mu_rot);
                if (!mrot) {
                    ATH_MSG_WARNING("This is not a  MdtDriftCircleOnTrack!!! ");
                    continue;
                }
                //      mdtSegment = true;

                // get digit from segment
                const Muon::MdtPrepData* prd = mrot->prepRawData();

                // digit identifier
                Identifier id = prd->identify();
                int adc = prd->adc();
                int tdc = prd->tdc();
                m_mdt_adc.push_back(adc) ;
                m_mdt_tdc.push_back(tdc) ;
                m_mdt_segIndex.push_back(iseg) ;



                MuonFixedId fixid = m_idToFixedIdTool->idToFixedId(id);
                m_mdt_id.push_back(fixid.getIdInt()) ;
                std::string st = fixid.stationNumberToFixedStationString(fixid.stationName());
                int ml = fixid.mdtMultilayer();
                int la = fixid.mdtTubeLayer();
                int tb = fixid.mdtTube();
                ATH_MSG_INFO("SegmentIndex "<<iseg<<" station " << st << " eta " << fixid.eta() << " phi " << fixid.phi() << " ML " << ml << " Layer " << la
                                          <<" Tube "<<tb<< " MROT drift R " << mrot->driftRadius() << " drift Time "
                                          << mrot->driftTime() <<" ADC "<<adc<<" TDC "<<tdc<< " ROT error " << std::sqrt(error2));

                //int mdt_segIndex = iseg ;
                //ATH_MSG_INFO("SegmentIndex "<<iseg<<" MDT Segment Hit ID "<<id<<" ADC "<<adc<<" TDC "<<tdc);

            } // finish mdt hit loop
          else if (m_MuonIdHelper->isRpc(rot_id)) {
                ATH_MSG_INFO("ROT ID is RPC "<<rot_id);
                ++nr;
                                              }
          else if (m_MuonIdHelper->isTgc(rot_id)) {
                ATH_MSG_INFO("ROT ID is TGC "<<rot_id);
                ++nt;
                                              }
          else if (m_MuonIdHelper->isCsc(rot_id)) {
                ATH_MSG_INFO("ROT ID is Csc "<<rot_id);
                ++nc;
                                              }
          else if (m_MuonIdHelper->isMM(rot_id)) {
                ATH_MSG_INFO("ROT ID is MM "<<rot_id);
                ++nmm;
                                              }
          else if (m_MuonIdHelper->issTgc(rot_id)) {
                ATH_MSG_INFO("ROT ID is sTgc "<<rot_id);
                ++ns;
                                              }


        } // finish rot loop
  ATH_MSG_INFO("Segment "<<iseg<<", nMDT "<<nm<<", nRPC "<<nr<<", nTGC "<<nt<<", nMM "<<nmm<<", nsTgc "<<ns<<", nCSC "<<nc) ;
        
      }// finish segment loop

  
  
  // SG::ReadHandle<Muon::MdtPrepDataContainer> mdtPrepRawData(m_MdtPrepData, ctx);
  // ATH_MSG_INFO("Number of MdtPrepRawData : "<<mdtPrepRawData->size());
  if (!m_tree.fill(ctx)) return StatusCode::FAILURE;
  return StatusCode::SUCCESS;
}

StatusCode MuonSegmentReader::finalize()
{
  ATH_MSG_INFO("MuonSegmentReader :: Finalize + Matching");
  ATH_CHECK(m_tree.write());
  return StatusCode::SUCCESS;
}



int MuonSegmentReader::segtrackHitsOverlap( const Trk::Segment* seg1, const Trk::Track* trk)
{

  //Muon::MuonSegment *seg = (Muon::MuonSegment*) seg1; //Muon::MuonSegment(seg1);
  const Muon::MuonSegment* seg = dynamic_cast<const Muon::MuonSegment*>(seg1);

  //  const Muon::MuonSegment *seg = dynamic_cast<const Muon::MuonSegment*>(seg1);
  if (seg->numberOfContainedROTs() == 0) {
            ATH_MSG_DEBUG(" Oops, segment without hits!!! ");
            return 0;
        }
  std::vector<const Trk::RIO_OnTrack*> rots; // new std::vector<const Trk::RIO_OnTrack*>(seg->numberOfContainedROTs()) ;
  std::vector<const Trk::RIO_OnTrack*> trots;
  trots.clear();
  for (unsigned int irot = 0; irot < seg->numberOfContainedROTs(); irot++) {
            // use pointer to rot
            const Trk::RIO_OnTrack* rot = seg->rioOnTrack(irot);
            rots.push_back(rot);
  }

  const DataVector<const Trk::MeasurementBase>* trk_mT = trk->measurementsOnTrack();
  for (int i = 0; i < trk_mT->size(); i++){
    
    const Trk::RIO_OnTrack* rio = dynamic_cast<const Trk::RIO_OnTrack*>(trk_mT->at(i));
    trots.push_back(rio);
  }
  
  if( rots.empty() || trots.empty()) {
    //if( m_debug ) m_log << MSG::DEBUG << " Oops, segment without hits!!! " << endreq;
    return 0;
  }


  std::vector<bool> vcount;
  for (int i = 0; i < int(rots.size()); i++){
    vcount.push_back(false);
    const Trk::RIO_OnTrack *rot = (rots[i]);
    for (int j = 0; j < int(trots.size()); j++){
      const Trk::RIO_OnTrack *trot = (trots[j]);
      if (trot != 0 && rot->identify() == trot->identify())
	    vcount[i] = true;
    }
  }

  int count = 0;
  for (int i = 0; i < vcount.size(); i++)
    if (vcount[i])
      count++;
  return count;
  
}





} // namespace MuonCalib