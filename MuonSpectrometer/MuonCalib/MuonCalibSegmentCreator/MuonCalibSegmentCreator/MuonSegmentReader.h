/*
    2  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
    3*/
#ifndef MUONCALIB_MUONSEGMENTREADER_H
#define MUONCALIB_MUONSEGMENTREADER_H
#include "AthenaBaseComps/AthHistogramAlgorithm.h"
//#include "xAODTracking/TrackParticleContainer.h"
#include <xAODEventInfo/EventInfo.h>
#include "MuonSegment/MuonSegment.h"
#include "MuonPrepRawData/MuonPrepDataContainer.h"

#include "TrkSegment/SegmentCollection.h"
#include "TrkTrack/TrackCollection.h"
#include "TrkExInterfaces/IExtrapolator.h"
#include "TrkToolInterfaces/IResidualPullCalculator.h"

//#include "LumiBlockData/LuminosityCondData.h"
#include "MuonCablingData/MuonMDT_CablingMap.h"
#include "AthenaBaseComps/AthAlgorithm.h"
#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/ToolHandle.h"
#include "MdtCalibSvc/MdtCalibrationTool.h"
// #include "MuonCalibEvent/MuonCalibPatternCollection.h"
#include "MuonCalibITools/IIdToFixedIdTool.h"
#include "MuonIdHelpers/IMuonIdHelperSvc.h"
#include "MuonReadoutGeometry/MuonDetectorManager.h"
#include "MuonTesterTree/MuonTesterTreeDict.h"
#include "StoreGate/ReadHandleKey.h"
#include "StoreGate/WriteHandleKey.h"

#include "TTree.h"

/** An example algorithm that reads and writes objects from the event store
using handles.*/
namespace Trk {
    //class IExtrapolator;
    class IResidualPullCalculator;
}  // namespace Trk


namespace MuonCalib {

// class MuonSegmentReader : public AthReentrantAlgorithm
//    {
//    public:
//    using AthReentrantAlgorithm::AthReentrantAlgorithm;
class MuonSegmentReader : public AthHistogramAlgorithm
   {
   public:
   using AthHistogramAlgorithm::AthHistogramAlgorithm;

   //virtual StatusCode initialize() override;
   virtual StatusCode initialize() override;
   // virtual StatusCode execute (const EventContext& ctx) const override;
   virtual StatusCode execute () override;
   virtual StatusCode finalize () override;
   
   unsigned int cardinality() const override final { return 1; } ;

   private:

        int segtrackHitsOverlap( const Trk::Segment* seg1, const Trk::Track* trk) ;

        SG::ReadHandleKey<xAOD::EventInfo> m_evtKey{this, "EventInfoKey", "EventInfo", "xAOD::EventInfo ReadHandleKey"};
        

        SG::ReadHandleKey<Trk::SegmentCollection> m_TrkSegKey {this, "MuonSegmentLocations", "TrackMuonSegments"};
        SG::ReadHandleKey<TrackCollection> m_TrkKey {this, "MuonTrackLocations", "MuonSpectrometerTracks"};

        SG::ReadHandleKey<Muon::MdtPrepDataContainer> m_MdtPrepData {this, "MdtPrepData", "MDT_DriftCircles"};

        // /** MuonDetectorManager from the conditions store */
        SG::ReadCondHandleKey<MuonGM::MuonDetectorManager> m_DetectorManagerKey{this, "DetectorManagerKey", "MuonDetectorManager",
                                                                                 "Key of input MuonDetectorManager condition data"};

        ServiceHandle<Muon::IMuonIdHelperSvc> m_MuonIdHelper{this, "MuonIdHelper", "Muon::MuonIdHelperSvc/MuonIdHelperSvc",
                                                         "Handle to the MuonIdHelperSvc"};

        // SG::ReadCondHandleKey<MuonMDT_CablingMap> m_cablingKey{this, "MDTCablingKey", "MuonMDT_CablingMap", "Key of output MDT cabling map"};


       
         /** pointer to MdtCalibSvc */
        ToolHandle<MdtCalibrationTool> m_calibrationTool{this, "CalibrationTool", "MdtCalibrationTool"};

        PublicToolHandle<Trk::IResidualPullCalculator> m_pullCalculator{this, "PullCalculator",
                                                                    "Trk::ResidualPullCalculator/ResidualPullCalculator"};


        // /** IdentifierTool initialization */
        // ToolHandle<Muon::IMuonPatternSegmentAssociationTool> m_assocTool{
        //     this, "PatternSegmentAssociationTool", "Muon::MuonPatternSegmentAssociationTool/MuonPatternSegmentAssociationTool"};
        ToolHandle<MuonCalib::IIdToFixedIdTool> m_idToFixedIdTool{this, "IdToFixedIdTool", "MuonCalib::IdToFixedIdTool/MuonCalib_IdToFixedIdTool"};

        
        MuonTesterTree m_tree{"Segments", "CALIBNTUPLESTREAM"};
        // book event_x branches
        ScalarBranch<int>& m_runNumber{m_tree.newScalar<int>("event_runNumber")};
        ScalarBranch<int>& m_eventNumber{m_tree.newScalar<int>("event_eventNumber")};
        //ScalarBranch<int>& m_nEvent{m_tree.newScalar<int>("event_nEvent")};
        ScalarBranch<int>& m_lumiBlock{m_tree.newScalar<int>("event_lumiBlock")};
        ScalarBranch<int>& m_bcId{m_tree.newScalar<int>("event_bcId")};
        ScalarBranch<int>& m_timeStamp{m_tree.newScalar<int>("event_timeStamp")};
        //ScalarBranch<unsigned int>& m_eventTag{m_tree.newScalar<unsigned int>("event_eventTag")};


        // int m_lbNumber = 0;
        // int m_eventNumber = 0;
        // int m_bcid = 0; 
        // int m_event_nEvent= 0;	 //number of events in the ntuple
        // // event_bcId	event bunch crossing number
        // event_lumiBlock	event luminosity block number
        // event_eventNumber	event number
        // event_runNumber	run number
        // event_timeStamp	time stamp of the event (empty at the moment, will be used for real data)
        // event_on_TAV_bits	trigger bits
        // event_n_on_TAV_bits	trigger bits
        // event_n_on_TAP_bits	trigger bits
        // event_on_TAP_bits	trigger bits
        // event_eventTag

        // Muon Segments branches
        //int m_seg_nSegments = 0;
        ScalarBranch<int>& m_seg_nSegments{m_tree.newScalar<int>("seg_nSegments")};
        VectorBranch<int>& m_seg_nHits{m_tree.newVector<int>("seg_nHits")};        
        // VectorBranch<int>& m_seg_nMdtHits{m_tree.newVector<int>("seg_nMdtHits")};        
        // VectorBranch<int>& m_seg_nRpcHits{m_tree.newVector<int>("seg_nRpcHits")};        
        // VectorBranch<int>& m_seg_nCscHits{m_tree.newVector<int>("seg_nCscHits")};        
        // VectorBranch<int>& m_seg_nTgcHits{m_tree.newVector<int>("seg_nTgcHits")};        
        // VectorBranch<int>& m_seg_nMMHits{m_tree.newVector<int>("seg_nMMHits")};        
        // VectorBranch<int>& m_seg_nsTgcHits{m_tree.newVector<int>("seg_nsTgcHits")};        

        //std::vector<int> m_seg_nHits;
        // std::vector<int> m_seg_nMdtHits;
        // std::vector<int> m_seg_nRpcHits;
        // std::vector<int> m_seg_nCscHits;
        // std::vector<int> m_seg_nTgcHits;

        // Segment Hit branches
        //int m_mdt_nMdt;	//number of MDT hits in the event
        //ScalarBranch<int>& m_mdt_nMdt{m_tree.newScalar<int>("mdt_nMdt")};
        VectorBranch<int>& m_mdt_segIndex{m_tree.newVector<int>("mdt_segIndex")};        
        VectorBranch<int>& m_mdt_adc{m_tree.newVector<int>("mdt_adc")};        
        VectorBranch<int>& m_mdt_tdc{m_tree.newVector<int>("mdt_tdc")};   
        VectorBranch<unsigned int>& m_mdt_id{m_tree.newVector<unsigned int>("mdt_id")};       

        // std::vector<int> m_mdt_segIndex;	//index telling the hit to which segment it belongs
        // std::vector<Identifier> m_mdt_id;	//identifier of the hit (given by MuonFixedId)
        // std::vector<int> m_mdt_tdc;	//tdc counts of the hit
        // std::vector<int> m_mdt_adc; 	//adc counts of the hit

        // Muon Track branches
        ScalarBranch<int>& m_trk_nTracks{m_tree.newScalar<int>("trk_nTracks")};
        //int m_trk_nTracks = 0;

        // Muon Track Hit branches
        ScalarBranch<int>& m_trkHit_nHits{m_tree.newScalar<int>("trkHit_nHits")};
        //std::vector<int> m_trkHit_trackIndex ;
        VectorBranch<int>& m_trkHit_trackIndex{m_tree.newVector<int>("trkHit_trackIndex")};        

        //std::vector<float> m_trkHit_driftRadius ;
        //std::vector<float> m_trkHit_driftTime ;
        //std::vector<float> m_trkHit_distRO ;       


        //MdtIdentifierBranch m_trkHit_id{m_tree, "trkHit_identifer", m_MuonIdHelper.get()};
        ThreeVectorBranch m_trkHit_pos{m_tree,"trkHit_pos"} ;
        VectorBranch<int>& m_trkHit_adc{m_tree.newVector<int>("trkHit_adc")};        
        VectorBranch<int>& m_trkHit_tdc{m_tree.newVector<int>("trkHit_tdc")};        
        VectorBranch<float>& m_trkHit_resi{m_tree.newVector<float>("trkHit_resi")};        
        VectorBranch<float>& m_trkHit_error{m_tree.newVector<float>("trkHit_error")};        
        VectorBranch<float>& m_trkHit_pull{m_tree.newVector<float>("trkHit_pull")};        
        VectorBranch<unsigned int>& m_trkHit_id{m_tree.newVector<unsigned int>("trkHit_id")};       
        VectorBranch<float>& m_trkHit_driftRadius{m_tree.newVector<float>("trkHit_driftRadius")};        
        VectorBranch<float>& m_trkHit_driftTime{m_tree.newVector<float>("trkHit_driftTime")};        
        VectorBranch<float>& m_trkHit_distRO{m_tree.newVector<float>("trkHit_distRO")};  
        //std::vector<float> m_trkHit_error ;
        //std::vector<float> m_trkHit_resi ;
        //std::vector<float> m_trkHit_pull ;
        //std::vector<int> m_trkHit_adc ;
        //std::vector<int> m_trkHit_tdc ;


        

        


   };
   
   }  // namespace MuonCalib
#endif